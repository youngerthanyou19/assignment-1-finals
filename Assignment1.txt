1) what are the new feature of IOS 8 for Developers?

-Touch ID
For the first time, you have the option of using Touch ID to sign in to third-party apps � there�s no need to enter a password. Your fingerprint data is protected and is never accessed by iOS or other apps.

-PhotoKit
Developers can enable their photo apps to edit photos directly in the Camera Roll without having to import them first.

-HealthKit
Health and fitness apps can share their data with the new Health app and with each other. When apps can talk to each other, they work harder for your health.

-HomeKit
HomeKit introduces a new way for you to control supported devices in your home using Siri. That means you could use your voice to turn on lights or adjust the thermostat.

-Camera API
Now third-party camera apps can have precise control over exposure, focus, and white balance in addition to the controls they already have.

-CloudKit
Now developers can use the full power of iCloud in their apps and enable you to sign in with your Apple ID without sharing your personal information.

-SpriteKit
SpriteKit enables developers to create high-performance, battery-efficient 2D games. In iOS 8, we�ve added several enhancements that will make 2D games even better. These new technologies will help in-game characters move more naturally and make it easier for developers to add force fields, detect collisions, and generate new lighting effects in their games.

-SceneKit
SceneKit enables developers to render game scenes in 3D and is designed for casual 3D gaming. SceneKit incorporates a physics engine, a particle generator, and easy ways to script the actions of 3D objects. It�s also completely integrated with SpriteKit, so developers can include SpriteKit assets in 3D games.

-Metal
Built for developers who create highly immersive console games, Metal is a new technology that allows them to squeeze maximum performance from the A7 and A8 chips. It�s optimized to allow the CPU and GPU to work together to achieve optimal performance. It�s designed for multithreading, and there are great tools for putting it all together in Xcode.*

-Swift (Proramming Language)
To spur the next generation of iOS apps, we�re introducing something entirely new: A programming language that�s as powerful as it is expressive and intuitive to use. For app developers, it means a new level of creative freedom. For you, it means a new level of app.